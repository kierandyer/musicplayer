package uk.co.hl.musicplayer.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uk.co.hl.musicplayer.model.Music;
import uk.co.hl.musicplayer.model.Playlist;
import uk.co.hl.musicplayer.repository.MusicPlayerRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MusicPlayerService {

    private final MusicPlayerRepository musicPlayerRepository;

    public void insertFavourites(String title, String album, String artist) {

        musicPlayerRepository.insert(title, album, artist);
    }

    public List<Music> searchFavourites(String searchItem) {
        return musicPlayerRepository.search(searchItem);

    }

    public List<Music> getAllMusicSortByTitle() {
        return musicPlayerRepository.getAllMusicSortByTitle();
    }

    public List<Music> getAllMusicSortByAlbum() {
        return musicPlayerRepository.getAllMusicSortByAlbum();
    }

    public void insertPlaylist(String name, String description) {

        musicPlayerRepository.insertPlaylist(name, description);
    }

    public List<Playlist> getAllPlaylists() {

        return musicPlayerRepository.getAllPlaylists();
    }

    public List<Music> randomShuffle() {

        return musicPlayerRepository.randomShuffle();

    }
}
