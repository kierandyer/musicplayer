package uk.co.hl.musicplayer.model;

import lombok.Data;

import java.util.List;

@Data
public class Playlist {

    private String name;
    private String description;
    private List<Music> music;
}
