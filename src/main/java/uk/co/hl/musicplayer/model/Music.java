package uk.co.hl.musicplayer.model;

import lombok.Data;

@Data
public class Music {

    private String title;
    private String album;
    private String artist;
    private String search;

}
