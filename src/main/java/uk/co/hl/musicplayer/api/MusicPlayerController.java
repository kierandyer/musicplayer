package uk.co.hl.musicplayer.api;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import uk.co.hl.musicplayer.domain.MusicPlayerService;
import uk.co.hl.musicplayer.model.Music;
import uk.co.hl.musicplayer.model.Playlist;

import java.util.List;

@Slf4j
@Controller
@Api(tags = {"Music Player"})
@RequiredArgsConstructor
public class MusicPlayerController {

    private final MusicPlayerService musicPlayerService;

    @GetMapping("/player")
    public String playerForm(Model model) {

        model.addAttribute("player", new Music());

        return "player";
    }

    @PostMapping("/player")
    public String playerSubmit(@ModelAttribute Music music) {

        musicPlayerService.insertFavourites(music.getTitle(), music.getAlbum(), music.getArtist());

        return "playerresult";
    }

    @GetMapping("/favourites")
    public String favourites(@ModelAttribute Music music, Model model) {

        List<Music> allMusic = musicPlayerService.getAllMusicSortByTitle();

        model.addAttribute("favourites", new Music());
        model.addAttribute("allMusic", allMusic);

        return "favourites";
    }

    @GetMapping("/favourites/album")
    public String favouritesAlbumSort(@ModelAttribute Music music, Model model) {

        List<Music> allMusic = musicPlayerService.getAllMusicSortByAlbum();

        model.addAttribute("favourites", new Music());
        model.addAttribute("allMusic", allMusic);

        return "favouritesalbumsort";
    }


    @PostMapping("/favourites")
    public String favouritesSearchResult(@ModelAttribute Music music, Model model) {

        List<Music> searchResult = musicPlayerService.searchFavourites(music.getSearch());

        model.addAttribute("searchResult", searchResult);

        return "favouritesresult";

    }

    @GetMapping("/playlist")
    public String playlistForm(@ModelAttribute Playlist playlist, Model model) {

        List<Playlist> allPlaylists = musicPlayerService.getAllPlaylists();

        model.addAttribute("playlist", new Playlist());
        model.addAttribute("allPlaylists", allPlaylists);

        return "playlist";
    }

    @PostMapping("/playlist")
    public String playlistSubmit(@ModelAttribute Playlist playlist) {

        musicPlayerService.insertPlaylist(playlist.getName(), playlist.getDescription());

        return "playlistresult";
    }

    @GetMapping("/shuffle")
    public String shuffle(Model model) {

        List<Music> randomShuffle = musicPlayerService.randomShuffle();

        model.addAttribute("randomshuffle", randomShuffle);

        return "shuffle";
    }

//    @PostMapping("/shuffle")
//    public String shuffle() {
//        return "shuffleresult";
//    }
}
