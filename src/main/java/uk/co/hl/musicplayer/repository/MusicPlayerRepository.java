package uk.co.hl.musicplayer.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import uk.co.hl.musicplayer.model.Music;
import uk.co.hl.musicplayer.model.Playlist;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@RequiredArgsConstructor
@Repository
public class MusicPlayerRepository {

    private static final String DB_URL = "jdbc:h2:file:~/IdeaProjects/MusicPlayer/src/main/resources/db/h2";
    private static final String DB_USERNAME = "";
    private static final String DB_PASSWORD = "";
    private static final String DB_DRIVER = "org.h2.Driver";

    public void insert(String title, String album, String artist) {

        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "INSERT INTO MUSIC (TITLE, ALBUM, ARTIST) VALUES ('" +
                    title + "', '" + album + "', '" + artist + "')";

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Music> search(String searchItem) {

        Connection conn = null;
        Statement stmt = null;
        List<Music> listResult = new ArrayList<>();

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "SELECT TITLE, ALBUM, ARTIST FROM MUSIC WHERE TITLE LIKE '%"
                    + searchItem + "%' OR ALBUM LIKE '%"
                    + searchItem + "%' OR ARTIST LIKE '%"
                    + searchItem + "%'";

            resultSetToMusicObject(stmt, listResult, sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return listResult;
    }

    public List<Music> getAllMusicSortByTitle() {

        Connection conn = null;
        Statement stmt = null;
        List<Music> listResult = new ArrayList<>();

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "SELECT TITLE, ALBUM, ARTIST FROM MUSIC ORDER BY TITLE";

            resultSetToMusicObject(stmt, listResult, sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return listResult;
    }

    public List<Music> getAllMusicSortByAlbum() {

        Connection conn = null;
        Statement stmt = null;
        List<Music> listResult = new ArrayList<>();

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "SELECT TITLE, ALBUM, ARTIST FROM MUSIC ORDER BY ALBUM";

            resultSetToMusicObject(stmt, listResult, sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return listResult;
    }

    public void insertPlaylist(String name, String description) {

        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "INSERT INTO PLAYLIST(PLAYLIST_NAME, DESCRIPTION) VALUES ('" +
                    name + "', '" + description + "')";

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Playlist> getAllPlaylists() {
        Connection conn = null;
        Statement stmt = null;
        List<Playlist> listResult = new ArrayList<>();

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "SELECT *" +
                    "FROM PLAYLIST" +
                    "         INNER JOIN PLAYLISTDETAILS ON PLAYLIST.ID = PLAYLISTDETAILS.PLAYLIST_ID" +
                    "         INNER JOIN MUSIC ON MUSIC.ID = PLAYLISTDETAILS.MUSIC_ID";

            ResultSet resultSet = stmt.executeQuery(sql);

            while (resultSet.next()) {
                Playlist playlist = new Playlist();
                playlist.setName(resultSet.getString("name"));
                playlist.setDescription(resultSet.getString("description"));
                listResult.add(playlist);
            }
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return listResult;
    }

    public List<Music> randomShuffle() {

        Connection conn = null;
        Statement stmt = null;
        List<Music> listResult = new ArrayList<>();

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "SELECT TITLE, ALBUM, ARTIST FROM MUSIC ORDER BY RAND()";

            resultSetToMusicObject(stmt, listResult, sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return listResult;
    }

    private void resultSetToMusicObject(Statement stmt, List<Music> listResult, String sql) throws SQLException {
        ResultSet resultSet = stmt.executeQuery(sql);

        while (resultSet.next()) {
            Music music = new Music();
            music.setTitle(resultSet.getString("title"));
            music.setAlbum(resultSet.getString("album"));
            music.setArtist(resultSet.getString("artist"));
            listResult.add(music);
        }
        resultSet.close();
    }

    private int randomID() {
        Connection conn = null;
        Statement stmt = null;
        int max = 0;

        try {
            Class.forName(DB_DRIVER);

            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = conn.createStatement();

            String sql = "SELECT MAX(ID) FROM MUSIC";

            ResultSet resultSet = stmt.executeQuery(sql);

            max = (int) resultSet.getObject(1);
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ThreadLocalRandom.current().nextInt(1, max);
    }
}
